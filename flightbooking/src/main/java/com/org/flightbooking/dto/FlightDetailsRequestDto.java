package com.org.flightbooking.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FlightDetailsRequestDto {

	private String source;
	private String destination;
	private String date;

}
