package com.org.flightbooking.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class ChargeDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long chargeId;

	private String flightCode;

	private String type;

	private double charges;

}
