package com.org.flightbooking.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class FlightDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long flightId;

	@Column(unique = true)
	private String flightCode;

	private String flightName;

	private String source;
	private String destination;
	private String day;
	private String fromTime;
	private String toTime;
	private String availability;
	private int seatsAvailable;

}
